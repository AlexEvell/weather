package com.alex.weather.async;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.alex.weather.controller.DayJSONWeatherParser;
import com.alex.weather.controller.DayWeatherHttpClient;
import com.alex.weather.fragment.DayFragment;
import com.alex.weather.model.Location;

/**
 * Created by Alex on 26.04.2015.
 */
public class DayAsync extends AsyncTask<String, Void, Location> {

    private DayFragment dayFragment;
    private ProgressDialog waitingDialog;

    public DayAsync(DayFragment dayFragment){
        this.dayFragment = dayFragment;
    }

    @Override
    protected void onPreExecute() {
        waitingDialog = new ProgressDialog(dayFragment.getActivity());
        waitingDialog.setIndeterminate(true);
        waitingDialog.setMessage("Connecting...");
        waitingDialog.setCancelable(false);
        waitingDialog.show();
    }

    @Override
    protected Location doInBackground(String... params) {
            Location location = null;
            String data = ( (new DayWeatherHttpClient()).getWeatherData(params[0]));
            if (data == null)
            return null;
            location = DayJSONWeatherParser.getWeather(data);
            return location;

            }

    @Override
    protected void onPostExecute(Location location) {
            super.onPostExecute(location);

            if (location == null)
            Toast.makeText(dayFragment.getActivity(), "Connect error", Toast.LENGTH_SHORT).show();
            else
                dayFragment.updateAdapter(location);
                waitingDialog.dismiss();
            }

    }