package com.alex.weather.async;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.alex.weather.controller.HourJSONWeatherParser;
import com.alex.weather.controller.HourWeatherHttpClient;
import com.alex.weather.fragment.HourFragment;
import com.alex.weather.model.Location;

/**
 * Created by Alex on 26.04.2015.
 */
public class HourAsync extends AsyncTask<String, Void, Location> {

    private HourFragment hourFragment;
    private ProgressDialog waitingDialog;

    public HourAsync(HourFragment hourFragment){
        this.hourFragment = hourFragment;
    }

    @Override
    protected void onPreExecute() {
        waitingDialog = new ProgressDialog(hourFragment.getActivity());
        waitingDialog.setIndeterminate(true);
        waitingDialog.setMessage("Connecting...");
        waitingDialog.setCancelable(false);
        waitingDialog.show();
    }

    @Override
    protected Location doInBackground(String... params) {
        Location location = null;
        String data = ( (new HourWeatherHttpClient()).getWeatherData(params[0]));
        if (data == null)
        return null;
        location = HourJSONWeatherParser.getWeather(data);
        return location;

    }

    @Override
    protected void onPostExecute(Location location) {
        super.onPostExecute(location);

        if (location == null)
            Toast.makeText(hourFragment.getActivity(), "Connect error", Toast.LENGTH_SHORT).show();
        else {
            hourFragment.updateAdapter(location);
            waitingDialog.dismiss();
        }
    }

}