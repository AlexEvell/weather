package com.alex.weather.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Alex on 25.04.2015.
 */
public class Location implements Serializable {

    private float longitude;
    private float latitude;
    private long cod;
    private long id;
    private String country;
    private String city;
    private ArrayList<Weather> weathers;

    public float getLongitude() {
        return longitude;
    }
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
    public float getLatitude() {
        return latitude;
    }
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
    public long getCod() {
        return cod;
    }
    public void setCod(long sunset) {
        this.cod = sunset;
    }
    public long getId() {
        return id;
    }
    public void setId(long sunrise) {
        this.id = sunrise;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public ArrayList<Weather> getWeathers() {
        return weathers;
    }

    public void setWeathers(ArrayList<Weather> weathers) {
        this.weathers = weathers;
    }
}
