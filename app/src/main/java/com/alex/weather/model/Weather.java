package com.alex.weather.model;

import android.graphics.Bitmap;

/**
 * Created by Alex on 25.04.2015.
 */
public class Weather {

    private int date;
    private float temperature;
    private float temperatureMin;
    private float temperatureMax;
    private float temperatureNight;
    private float temperatureEvening;
    private float temperatureMorning;
    private float pressure;
    private float seaLevel;
    private float grndLevel;
    private int humidity;
    private int idWeather;
    private String mainWeather;
    private String description;
    private String icon;
    private int all;
    private float speed;
    private float deg;
    private String pod;
    private String dateText;
    private Bitmap iconData;
    private int cloud;
    private float rain;

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(float temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public float getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(float temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(float seaLevel) {
        this.seaLevel = seaLevel;
    }

    public float getGrndLevel() {
        return grndLevel;
    }

    public void setGrndLevel(float grndLevel) {
        this.grndLevel = grndLevel;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getIdWeather() {
        return idWeather;
    }

    public void setIdWeather(int idWeather) {
        this.idWeather = idWeather;
    }

    public String getMainWeather() {
        return mainWeather;
    }

    public void setMainWeather(String mainWeather) {
        this.mainWeather = mainWeather;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getDateText() {
        return dateText;
    }

    public void setDateText(String dateText) {
        this.dateText = dateText;
    }

    public Bitmap getIconData() {
        return iconData;
    }

    public void setIconData(Bitmap iconData) {
        this.iconData = Bitmap.createScaledBitmap(iconData, 90, 90, false);
    }

    public float getTemperatureMorning() {
        return temperatureMorning;
    }

    public void setTemperatureMorning(float temperatureMorning) {
        this.temperatureMorning = temperatureMorning;
    }

    public float getTemperatureNight() {
        return temperatureNight;
    }

    public void setTemperatureNight(float temperatureNight) {
        this.temperatureNight = temperatureNight;
    }

    public float getTemperatureEvening() {
        return temperatureEvening;
    }

    public void setTemperatureEvening(float temperatureEvening) {
        this.temperatureEvening = temperatureEvening;
    }

    public int getCloud() {
        return cloud;
    }

    public void setCloud(int cloud) {
        this.cloud = cloud;
    }

    public float getRain() {
        return rain;
    }

    public void setRain(float rain) {
        this.rain = rain;
    }
}