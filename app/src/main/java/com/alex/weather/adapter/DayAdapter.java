package com.alex.weather.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.alex.weather.R;
import com.alex.weather.model.Weather;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Alex on 26.04.2015.
 */
public class DayAdapter extends BaseAdapter {

    private ArrayList<Weather> weathers;
    private Context context;
    private LayoutInflater inflater;
    private MaterialEditText description, temp, hour, night;
    private View view;
    private ImageView icon;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
    //private String[] namesOfDays = DateFormatSymbols.getInstance().getWeekdays();
    private Calendar cal;
    private String[] namesOfDays = {"", "Sunday", "Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Saturday"};

    public DayAdapter(Context context, ArrayList<Weather> weathers) {
        this.weathers = weathers;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        cal = Calendar.getInstance();
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        /*
        for (int i = 1; i < namesOfDays.length; i++)
            namesOfDays[i] = namesOfDays[i].substring(0, 1).toUpperCase() + namesOfDays[i].substring(1);
        */
    }

    @Override
    public int getCount() {
        return weathers.size();
    }

    @Override
    public Object getItem(int position) {
        return weathers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        view = inflater.from(context).inflate(R.layout.adapter_day, null, false);

        //   description = (MaterialAutoCompleteTextView)view.findViewById(R.id.description);
        temp = (MaterialEditText)view.findViewById(R.id.temp);
        night = (MaterialEditText)view.findViewById(R.id.night);
        hour = (MaterialEditText)view.findViewById(R.id.hour);
        icon = (ImageView)view.findViewById(R.id.icon);
        cal.setTime(new Date(weathers.get(position).getDate() * (long) 1000));

        // description.setText(weathers.get(position).getMainWeather() + " : " + weathers.get(position).getDescription());
        temp.setText(Math.round(weathers.get(position).getTemperature() - 273.15) + " \u2103");
        night.setText(Math.round(weathers.get(position).getTemperatureNight() - 273.15) + " \u2103");
        hour.setText(namesOfDays[cal.get(Calendar.DAY_OF_WEEK)]);
        icon.setImageBitmap(weathers.get(position).getIconData());
        return view;
    }

}
