package com.alex.weather.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.alex.weather.R;
import com.alex.weather.async.DayAsync;
import com.alex.weather.async.HourAsync;
import com.alex.weather.fragment.DayFragment;
import com.alex.weather.fragment.HourFragment;
import com.alex.weather.sliding.SlidingTabLayout;
import com.alex.weather.sliding.ViewPagerAdapter;


public class MainActivity extends ActionBarActivity {

    private SlidingTabLayout slidingTabLayout;
    private ViewPager pager;
    private Toolbar toolbar;
    private ViewPagerAdapter viewPagerAdapter;
    private TextView city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (android.support.v7.widget.Toolbar) this.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        city = (TextView)findViewById(R.id.city);
        pager = (ViewPager) findViewById(R.id.pager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(viewPagerAdapter);

        slidingTabLayout.setViewPager(pager);
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return Color.WHITE;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        new AlertDialog.Builder(this)
                .setTitle("Refresh")
                .setMessage("Are you sure you want to refresh this weather?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String cityTask = "707471";
                        new HourAsync((HourFragment) viewPagerAdapter.getItem(0)).execute(cityTask);
                        new DayAsync((DayFragment) viewPagerAdapter.getItem(1)).execute(cityTask);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
        return super.onOptionsItemSelected(item);
    }

    public void setCity(String location){
        city.setVisibility(View.VISIBLE);
        city.setText(location);
    }

}