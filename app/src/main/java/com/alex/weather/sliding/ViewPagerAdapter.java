package com.alex.weather.sliding;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.alex.weather.fragment.DayFragment;
import com.alex.weather.fragment.HourFragment;

/**
 * Created by Alex on 26.04.2015.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private String titles[] = {"Hour", "Days"} ;
    private HourFragment hourFragment;
    private DayFragment dayFragment;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        hourFragment = new HourFragment();
        dayFragment = new DayFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return hourFragment;
            case 1:
                return dayFragment;
        }
        return null;
    }

    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

}
