package com.alex.weather.controller;

import com.alex.weather.model.Location;
import com.alex.weather.model.Weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alex on 25.04.2015.
 */
public class HourJSONWeatherParser {

    public static Location getWeather(String data) {
        Location loc = new Location();
        try {
            JSONObject jObj = new JSONObject(data);
            JSONObject city = getObject("city", jObj);
            JSONObject coord = getObject("coord", city);

            loc.setId(getInt("id", city));
            loc.setCod(getInt("cod", jObj));
            loc.setCity(getString("name", city));
            loc.setCountry(getString("country", city));
            loc.setLatitude(getFloat("lat", coord));
            loc.setLongitude(getFloat("lon", coord));

            JSONArray list = jObj.getJSONArray("list");

            ArrayList<Weather> weathers = new ArrayList<>();
            for (int i = 0; i < list.length(); i++) {
                Weather weather = new Weather();
                JSONObject JSONWeather = list.getJSONObject(i);
                JSONObject main = getObject("main", JSONWeather);
                JSONArray weatherJSON = JSONWeather.getJSONArray("weather");
                JSONObject weatherObject = weatherJSON.getJSONObject(0);
                JSONObject clouds = getObject("clouds", JSONWeather);
                JSONObject wind = getObject("wind", JSONWeather);
                JSONObject sys = getObject("sys", JSONWeather);

                weather.setDate(getInt("dt", JSONWeather));
                weather.setTemperature(getFloat("temp", main));
                weather.setTemperatureMin(getFloat("temp_min", main));
                weather.setTemperatureMax(getFloat("temp_max", main));
                weather.setPressure(getFloat("pressure", main));
                weather.setSeaLevel(getFloat("sea_level", main));
                weather.setGrndLevel(getFloat("grnd_level", main));
                weather.setHumidity(getInt("humidity", main));
                weather.setIdWeather(getInt("id", weatherObject));
                weather.setMainWeather(getString("main", weatherObject));
                weather.setDescription(getString("description", weatherObject));
                weather.setIcon(getString("icon", weatherObject));
                weather.setAll(getInt("all", clouds));
                weather.setSpeed(getFloat("speed", wind));
                weather.setDeg(getFloat("deg", wind));
                weather.setPod(getString("pod", sys));
                weather.setDateText(getString("dt_txt", JSONWeather));
                weather.setIconData((new HourWeatherHttpClient()).getBitmapFromURL(weather.getIcon()));
                weathers.add(weather);
            }
            loc.setWeathers(weathers);

        } catch (Exception e) {
           return null;
        }
        //float massage = getFloat("message", jObj);

        return loc;
    }


    private static JSONObject getObject(String tagName, JSONObject jObj)  throws JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }

    private static String getString(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getString(tagName);
    }

    private static float  getFloat(String tagName, JSONObject jObj) throws JSONException {
        return (float) jObj.getDouble(tagName);
    }

    private static int  getInt(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getInt(tagName);
    }

}
