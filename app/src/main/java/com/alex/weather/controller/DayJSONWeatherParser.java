package com.alex.weather.controller;

import com.alex.weather.model.Location;
import com.alex.weather.model.Weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alex on 25.04.2015.
 */
public class DayJSONWeatherParser {

    public static Location getWeather(String data) {
        Location loc = new Location();
        try {
            JSONObject jObj = new JSONObject(data);
            JSONObject city = getObject("city", jObj);
            JSONObject coord = getObject("coord", city);

            loc.setId(getInt("id", city));
            loc.setCod(getInt("cod", jObj));
            loc.setCity(getString("name", city));
            loc.setCountry(getString("country", city));
            loc.setLatitude(getFloat("lat", coord));
            loc.setLongitude(getFloat("lon", coord));

            JSONArray list = jObj.getJSONArray("list");

            ArrayList<Weather> weathers = new ArrayList<>();
            for (int i = 0; i < list.length(); i++) {
                Weather weather = new Weather();
                JSONObject JSONWeather = list.getJSONObject(i);
                JSONObject temp = getObject("temp", JSONWeather);
                JSONArray weatherJSON = JSONWeather.getJSONArray("weather");
                JSONObject weatherObject = weatherJSON.getJSONObject(0);

                weather.setDate(getInt("dt", JSONWeather));
                weather.setTemperature(getFloat("day", temp));
                weather.setTemperatureMin(getFloat("min", temp));
                weather.setTemperatureMax(getFloat("max", temp));
                weather.setTemperatureNight(getFloat("night", temp));
                weather.setTemperatureEvening(getFloat("eve", temp));
                weather.setTemperatureMorning(getFloat("morn", temp));
                weather.setPressure(getFloat("pressure", JSONWeather));
                weather.setHumidity(getInt("humidity", JSONWeather));
                weather.setIdWeather(getInt("id", weatherObject));
                weather.setMainWeather(getString("main", weatherObject));
                weather.setDescription(getString("description", weatherObject));
                weather.setIcon(getString("icon", weatherObject));
                weather.setSpeed(getFloat("speed", JSONWeather));
                weather.setDeg(getFloat("deg", JSONWeather));
                weather.setCloud(getInt("clouds", JSONWeather));
                //weather.setRain(getFloat("rain", JSONWeather));
                weather.setIconData((new HourWeatherHttpClient()).getBitmapFromURL(weather.getIcon()));
                weathers.add(weather);
            }
            loc.setWeathers(weathers);

        } catch (Exception e) {
           return null;
        }

        return loc;
    }


    private static JSONObject getObject(String tagName, JSONObject jObj)  throws JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }

    private static String getString(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getString(tagName);
    }

    private static float  getFloat(String tagName, JSONObject jObj) throws JSONException {
        return (float) jObj.getDouble(tagName);
    }

    private static int  getInt(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getInt(tagName);
    }

}
