package com.alex.weather.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.alex.weather.R;
import com.alex.weather.activity.MainActivity;
import com.alex.weather.adapter.HourAdapter;
import com.alex.weather.async.HourAsync;
import com.alex.weather.model.Location;

/**
 * Created by Alex on 25.04.2015.
 */
public class HourFragment extends Fragment {

    private View view;
    private ListView hourList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_hour, null, false);

        String cityTask = "707471";
        hourList = (ListView)view.findViewById(R.id.hour_list);

        HourAsync task = new HourAsync(this);
        task.execute(new String[]{cityTask});

        return view;
    }

    public void updateAdapter(Location location){
        ((MainActivity)getActivity()).setCity(location.getCity() + ", " + location.getCountry());
        hourList.setAdapter(new HourAdapter(getActivity(), location.getWeathers()));
    }

}
